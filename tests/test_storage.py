from io import StringIO

from px4ctl.mission import Mission, Waypoint
from px4ctl.storage import write_mission, read_mission


def test_read_write():
    buffer = StringIO()
    mission = Mission(waypoints=(Waypoint(1, 1, 1), Waypoint(1, 2, 1)))

    write_mission(buffer, mission)
    buffer.seek(0)  # reset buffer to beginning for before reading

    assert len(buffer.getvalue()) != 0
    assert read_mission(buffer) == mission
