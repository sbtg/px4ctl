from px4ctl.commands import Command, CommandError
from pytest import raises, mark


@mark.asyncio
async def test_command_always_success():
    async def coroutine():
        return 10

    result = await Command(coroutine)
    assert result == 10


@mark.asyncio
async def test_command_eventually_success():
    async def fail_coroutine():
        raise RuntimeError()

    async def success_coroutine():
        return 10

    coroutines = [fail_coroutine(), success_coroutine()]
    coroutines_iter = iter(coroutines)

    result = await Command(lambda: next(coroutines_iter))
    assert result == 10


@mark.asyncio
async def test_command_never_success():
    async def coroutine():
        raise RuntimeError()

    with raises(CommandError):
        await Command(coroutine)
