from px4ctl import Coordinate, Waypoint, Mission
from pytest import raises


def test_coordinate_init():
    Coordinate(lat=40, lon=-127)
    Coordinate(lat=61, lon=12.345)

    with raises(TypeError):
        Coordinate(lat="f", lon=100)
        Coordinate(lat=[1, 2, 3], lon={"foo": 10})

    with raises(ValueError):
        Coordinate(lat=-91, lon=0)
        Coordinate(lat=0, lon=182)


def test_waypoint_init():
    Waypoint(lat=40, lon=10, alt=1)
    Waypoint(lat=-89, lon=73, alt=12.0)

    with raises(TypeError):
        Waypoint(lat=0, lon=0, alt="f")
        Waypoint(lat=0, lon=0, alt=[1, 2, 3])

    with raises(ValueError):
        Waypoint(lat=0, lon=0, alt=0)


def test_init_mission_list():
    waypoints = [Waypoint(lat=0, lon=0, alt=1)]
    mission = Mission(waypoints=waypoints)

    assert type(mission.waypoints) == tuple


def test_init_mission_tuple():
    waypoints = (Waypoint(lat=0, lon=0, alt=1),)
    mission = Mission(waypoints=waypoints)

    assert type(mission.waypoints) == tuple
