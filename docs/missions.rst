Missions
========

Missions are comprised of two elements, Waypoints and Geofences. Waypoints are a series of locations that the drone will
travel to in order, and geofences denote boundaries that the drone cannot cross. When a mission is created it is
immutable, so any methods which modify the waypoints or geofences returns an entire new mission object. A mission with
zero or more waypoints is a valid mission, but a mission must have at least one waypoint to be executable. A mission
with no geofences is still executable.

.. autoclass:: px4ctl.mission.Mission
    :members:

Waypoints
---------

A  waypoint is a vector of 3 elements: lat, lon and alt. The latitude and longitude are given in absolute values, while 
the altitude is given relative to the takeoff altitude.

.. autoclass:: px4ctl.mission.Waypoint
    :members:


Geofences
---------

A Geofence is a polygon that defines a boundary the drone may not cross. There are two types of geofences: exclusive and
inclusive. An exclusive fence should not be entered by a drone, and an inclusive fence may not be exited by a drone.

.. autoclass:: px4ctl.mission.FenceType
    :members:

.. autoclass:: px4ctl.mission.Geofence
    :members:
