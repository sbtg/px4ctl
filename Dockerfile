FROM python:3.8

RUN adduser px4ctl
USER px4ctl

COPY pyproject.toml /opt/px4ctl/
COPY src/ /opt/px4ctl/src/
RUN python3 -m pip install /opt/px4ctl

ENTRYPOINT ["python3", "-m", "px4ctl"]
