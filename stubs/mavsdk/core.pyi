from typing import AsyncIterable

class ConnectionState:
    is_connected: bool

class Core:
    def connection_state(self) -> AsyncIterable[ConnectionState]: ...
