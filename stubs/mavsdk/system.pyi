from typing import Optional

from .action import Action
from .core import Core
from .mission import Mission
from .telemetry import Telemetry
from .geofence import Geofence
from .param import Param

class System:
    action: Action
    core: Core
    mission: Mission
    telemetry: Telemetry
    geofence: Geofence
    param: Param
    def __init__(self, mavsdk_server_address: Optional[str] = ..., port: int = ...) -> None: ...
    async def connect(self, system_address: Optional[str] = ...) -> None: ...
