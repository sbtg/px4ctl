from enum import Enum
from typing import AsyncGenerator

class Position:
    latitude_deg: float
    longitude_deg: float
    absolute_altitude_m: float
    relative_altitude_m: float

class GroundTruth:
    latitude_deg: float
    longitude_deg: float
    absolute_altitude_m: float

class LandedState(Enum):
    UNKNOWN = ...
    ON_GROUND = ...
    IN_AIR = ...
    TAKING_OFF = ...
    LANDING = ...

class Health:
    is_global_position_ok: bool
    is_home_position_ok: bool

class PositionBody:
    x_m: float
    y_m: float
    z_m: float

class Quaternion: ...

class VelocityBody:
    def __init__(self, x_m_s: float, y_m_s: float, z_m_s: float): ...
    x_m_s: float
    y_m_s: float
    z_m_s: float

class AngularVelocityBody: ...
class Covariance: ...

class Odometry:
    time_usec: int
    frame_id: MavFrame
    child_frame_id: MavFrame
    position_body: PositionBody
    velocity_body: VelocityBody
    angular_velocity_body: AngularVelocityBody
    pose_covariance: Covariance
    velocity_covariance: Covariance
    class MavFrame(Enum):
        UNDEF = ...
        BODY_NED = ...
        VISION_NED = ...
        ESTIM_NED = ...

class Telemetry:
    def position(self) -> AsyncGenerator[Position, None]: ...
    def ground_truth(self) -> AsyncGenerator[GroundTruth, None]: ...
    def health(self) -> AsyncGenerator[Health, None]: ...
    def landed_state(self) -> AsyncGenerator[LandedState, None]: ...
    def odometry(self) -> AsyncGenerator[Odometry, None]: ...
    def unix_epoch_time(self) -> AsyncGenerator[int, None]: ...
