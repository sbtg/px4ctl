from typing import Optional

from pymavlink.mavutil import mavlink

class Command(mavlink.MAVlink_mission_item_message): ...

class CommandSequence:
    next: int
    def download(self) -> None: ...
    def wait_ready(self) -> None: ...
    def clear(self) -> None: ...
    def add(self, command: Command) -> None: ...
    def upload(self, timeout: Optional[int] = None) -> None: ...

class VehicleMode:
    def __init__(self, name: str): ...

class LocationLocal:
    north: float
    east: float
    down: float

class LocationGlobal:
    lat: float
    lon: float
    alt: float

class LocationGlobalRelative:
    lat: float
    lon: float
    alt: float

class Locations:
    @property
    def local_frame(self) -> LocationLocal: ...
    @property
    def global_frame(self) -> LocationGlobal: ...
    @property
    def global_relative_frame(self) -> LocationGlobalRelative: ...

# The message factory is the mav attribute of the Vehicle _master attribute, which is a MAVLink class
# This type can only be used during type checking since the actual package does not export a MessageFactory value

MessageFactory = mavlink.MAVLink

class Vehicle:
    commands: CommandSequence
    armed: bool
    mode: VehicleMode
    @property
    def is_armable(self) -> bool: ...
    @property
    def message_factory(self) -> MessageFactory: ...
    @property
    def location(self) -> Locations: ...
    def send_mavlink(self, message: object) -> None: ...
    def simple_takeoff(self, alt: Optional[float] = ...) -> None: ...

def connect(ip: str, wait_ready: bool = ...) -> Vehicle:
    pass
