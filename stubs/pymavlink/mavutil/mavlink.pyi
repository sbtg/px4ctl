from __future__ import annotations

from typing import Callable

MAV_FRAME_GLOBAL_RELATIVE_ALT: int = ...
MAV_CMD_NAV_TAKEOFF: int = ...
MAV_CMD_NAV_WAYPOINT: int = ...

class MAVLink_message: ...

class MAVlink_mission_item_message(MAVLink_message):
    def __init__(
        self,
        target_system: int,
        target_component: int,
        seq: int,
        frame: int,
        command: int,
        current: int,
        autocontinue: int,
        param1: int,
        param2: int,
        param3: int,
        param4: int,
        x: float,
        y: float,
        z: float,
        mission_type: int = ...,
    ): ...

class MAVLink:
    def __getattr__(self, name: str) -> Callable[..., MAVLink_message]: ...
